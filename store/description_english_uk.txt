Do you ever have missed important things in games like the blue buff in League of Legends? 
Then the Gameplay Timer is the right thing for you! You will get instantly notified, when something important happens in your game. 
You will not only remembered on things of one game, because the app is ready for more games. At the moment there are not many games, but if you have suggestions for one, send me a mail.

<font color="#CC0000"><b>Current Games:</b></font>
- League of Legends
- Dota 2
- Grand Theft Auto V
- Starcraft 2
- Speed Cubing World Records

If you want to help, you can take a look at the source code on GitLab as the app is open source.
https://gitlab.com/NicoAlt/gpt
