package de.nico.gpt;

/*
 * @author Nico Alt
 * @author Devin
 * See the file "LICENSE" for the full license governing this code.
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.GregorianCalendar;

@SuppressLint("NewApi")
public class MainGameActivity extends Activity {

    boolean mainisopen;
    boolean t1;
    boolean t2;
    boolean t3;

    long newtime;
    int delay = 1000;
    int ranint;
    int currentapiVersion;

    long lol_actualtimea1;
    long lol_actualtimea2;
    long lol_actualtimea3;
    long lol_actualtimea4;
    long lol_actualtimea5;
    long lol_actualtimeb1;
    long lol_actualtimeb2;
    long lol_actualtimeb3;
    long lol_actualtimeb4;
    long lol_actualtimeb5;

    long lol_newtime_a1;
    long lol_newtime_a2;
    long lol_newtime_a3;
    long lol_newtime_a4;
    long lol_newtime_a5;
    long lol_newtime_b1;
    long lol_newtime_b2;
    long lol_newtime_b3;
    long lol_newtime_b4;
    long lol_newtime_b5;

    long gtav_actualtimea1;
    long gtav_actualtimea2;
    long gtav_actualtimea3;

    long gtav_newtime_a1;
    long gtav_newtime_a2;
    long gtav_newtime_a3;

    long dota2_actualtimea1;
    long dota2_newtime_a1;

    long sc2_actualtimea1;
    long sc2_actualtimea2;
    long sc2_actualtimea3;
    long sc2_actualtimea4;

    long sc2_newtime_a1;
    long sc2_newtime_a2;
    long sc2_newtime_a3;
    long sc2_newtime_a4;

    long cube_actualtimea1;
    long cube_actualtimea2;
    long cube_actualtimea3;
    long cube_actualtimeb1;
    long cube_actualtimeb2;
    long cube_actualtimeb3;

    long cube_newtime_a1;
    long cube_newtime_a2;
    long cube_newtime_a3;
    long cube_newtime_b1;
    long cube_newtime_b2;
    long cube_newtime_b3;

    String buttonid;
    String buttontext;
    String stringtime;
    String minute;
    String minutes;
    String the1;
    String the2;
    String seconds;
    String shortname;

    String end1;
    String end2;
    String end3;
    String end4;
    String end5;
    String end6;
    String end7;

    String actualthinga1;
    String actualthinga2;
    String actualthinga3;
    String actualthinga4;
    String actualthinga5;
    String actualthingb1;
    String actualthingb2;
    String actualthingb3;
    String actualthingb4;
    String actualthingb5;

    String actualgame;

    boolean lol_timerHasStarted_a1 = false;
    boolean lol_timerHasStarted_a2 = false;
    boolean lol_timerHasStarted_a3 = false;
    boolean lol_timerHasStarted_a4 = false;
    boolean lol_timerHasStarted_a5 = false;
    boolean lol_timerHasStarted_b1 = false;
    boolean lol_timerHasStarted_b2 = false;
    boolean lol_timerHasStarted_b3 = false;
    boolean lol_timerHasStarted_b4 = false;
    boolean lol_timerHasStarted_b5 = false;

    boolean gtav_timerHasStarted_a1 = false;
    boolean gtav_timerHasStarted_a2 = false;
    boolean gtav_timerHasStarted_a3 = false;

    boolean dota2_timerHasStarted_a1 = false;

    boolean sc2_timerHasStarted_a1 = false;
    boolean sc2_timerHasStarted_a2 = false;
    boolean sc2_timerHasStarted_a3 = false;
    boolean sc2_timerHasStarted_a4 = false;

    boolean cube_timerHasStarted_a1 = false;
    boolean cube_timerHasStarted_a2 = false;
    boolean cube_timerHasStarted_a3 = false;
    boolean cube_timerHasStarted_b1 = false;
    boolean cube_timerHasStarted_b2 = false;
    boolean cube_timerHasStarted_b3 = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_game);
        setTitle("Gameplay Timer");
        mainisopen = true;

        // timer does not run
        t1 = false;
        t2 = false;
        t3 = false;
        ranint = 1;

        // text
        minute = getResources().getString(R.string.minute);
        shortname = getResources().getString(R.string.shortname);
        minutes = getResources().getString(R.string.minutes);
        seconds = getResources().getString(R.string.seconds);
        the1 = getResources().getString(R.string.the1);
        the2 = getResources().getString(R.string.the2);

        // end text for button
        end1 = getResources().getString(R.string.notifiy_end1);
        end2 = getResources().getString(R.string.notifiy_end2);
        end3 = getResources().getString(R.string.notifiy_end3);
        end4 = getResources().getString(R.string.notifiy_end4);
        end5 = getResources().getString(R.string.notifiy_end5);
        end6 = getResources().getString(R.string.notifiy_end6);
        end7 = getResources().getString(R.string.notifiy_end7);

        currentapiVersion = android.os.Build.VERSION.SDK_INT;
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Clear all notification
        NotificationManager nMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nMgr.cancelAll();
    }

	/*
     * @Override public boolean onCreateOptionsMenu(Menu menu) { // Inflate the
	 * menu; this adds items to the action bar if it is present.
	 * getMenuInflater().inflate(R.menu.main, menu); return true; }
	 * 
	 * @Override public boolean onOptionsItemSelected(MenuItem item) { // Handle
	 * presses on the action bar items switch (item.getItemId()) { case
	 * R.id.action_restart: Intent i = getBaseContext().getPackageManager()
	 * .getLaunchIntentForPackage( getBaseContext().getPackageName() );
	 * i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); startActivity(i); default:
	 * return super.onOptionsItemSelected(item); } }
	 */

    public void maingame(View v) {
        switch (v.getId()) {
            case (R.id.buttonmain4):
                setContentView(R.layout.game_lol);
                actualgame = getResources().getString(R.string.gamebuttonlol);
                setTitle(shortname + " - " + actualgame);
                mainisopen = false;
                if (lol_timerHasStarted_a1 == true) {
                    refreshLoL(1);
                }
                if (lol_timerHasStarted_a2 == true) {
                    refreshLoL(2);
                }
                if (lol_timerHasStarted_a3 == true) {
                    refreshLoL(3);
                }
                if (lol_timerHasStarted_a4 == true) {
                    refreshLoL(4);
                }
                if (lol_timerHasStarted_a5 == true) {
                    refreshLoL(5);
                }
                if (lol_timerHasStarted_b1 == true) {
                    refreshLoL(6);
                }
                if (lol_timerHasStarted_b2 == true) {
                    refreshLoL(7);
                }
                if (lol_timerHasStarted_b3 == true) {
                    refreshLoL(8);
                }
                if (lol_timerHasStarted_b4 == true) {
                    refreshLoL(9);
                }
                if (lol_timerHasStarted_b5 == true) {
                    refreshLoL(10);
                }
                break;
            case (R.id.buttonmain6):
                setContentView(R.layout.game_sc2);
                actualgame = getResources().getString(R.string.gamebuttonsc2);
                setTitle(shortname + " - " + actualgame);
                mainisopen = false;
                if (sc2_timerHasStarted_a1 == true) {
                    refreshSC2(1);
                }
                if (sc2_timerHasStarted_a2 == true) {
                    refreshSC2(2);
                }
                if (sc2_timerHasStarted_a3 == true) {
                    refreshSC2(3);
                }
                if (sc2_timerHasStarted_a4 == true) {
                    refreshSC2(4);
                }
                break;
            case (R.id.buttonmain2_5):
                setContentView(R.layout.game_dota2);
                actualgame = getResources().getString(R.string.gamebuttondota2);
                setTitle(shortname + " - " + actualgame);
                mainisopen = false;
                if (dota2_timerHasStarted_a1 == true) {
                    refreshDota2();
                }
                break;
            case (R.id.buttonmain3):
                setContentView(R.layout.game_gtav);
                actualgame = getResources().getString(R.string.gamebuttongtav);
                setTitle(shortname + " - " + actualgame);
                mainisopen = false;
                if (gtav_timerHasStarted_a1 == true) {
                    refreshGTAV(1);
                }
                if (gtav_timerHasStarted_a2 == true) {
                    refreshGTAV(2);
                }
                if (gtav_timerHasStarted_a3 == true) {
                    refreshGTAV(3);
                }
                break;
            case (R.id.buttonmain5):
                setContentView(R.layout.game_ps2_repu);
                actualgame = getResources().getString(R.string.gamebuttonps2);
                setTitle(shortname + " - " + actualgame);
                mainisopen = false;
                break;
            case (R.id.buttonmain2):
                setContentView(R.layout.game_cube);
                actualgame = getResources().getString(R.string.gamebuttoncube);
                setTitle(shortname + " - " + actualgame);
                mainisopen = false;
                if (cube_timerHasStarted_a1 == true) {
                    refreshCUBE(1);
                }
                if (cube_timerHasStarted_a2 == true) {
                    refreshCUBE(2);
                }
                if (cube_timerHasStarted_a3 == true) {
                    refreshCUBE(3);
                }
                if (cube_timerHasStarted_b1 == true) {
                    refreshCUBE(4);
                }
                if (cube_timerHasStarted_b2 == true) {
                    refreshCUBE(5);
                }
                if (cube_timerHasStarted_b3 == true) {
                    refreshCUBE(6);
                }
                break;
        }
    }

    public void timer(View v) {
        return;
    }

    public void gamecube(View v) {
        switch (v.getId()) {
            case (R.id.buttona1):
                cube_newtime_a1 = 6000;// 6 seconds
                cube_actualtimea1 = cube_newtime_a1;
                Toast.makeText(
                        getApplicationContext(),
                        (getResources().getString(R.string.game_cube_a1)) + ": "
                                + String.valueOf(cube_newtime_a1 / 1000) + " "
                                + seconds, Toast.LENGTH_SHORT).show();

                // set AlarmManager
                cube_newtime_a1 = new GregorianCalendar().getTimeInMillis()
                        + cube_newtime_a1;
                Intent intent_cube_a1 = new Intent(this, AlarmReciever.class);
                Bundle cube_extras_a1 = new Bundle();
                cube_extras_a1.putString("title",
                        (getResources().getString(R.string.game_cube_a1)));
                cube_extras_a1.putString(
                        "what",
                        the1 + " "
                                + (getResources().getString(R.string.game_cube_a1))
                                + " " + end7);
                intent_cube_a1.putExtras(cube_extras_a1);

                AlarmManager cube_alarmManager_a1 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                if (currentapiVersion >= android.os.Build.VERSION_CODES.KITKAT) {
                    cube_alarmManager_a1.setExact(AlarmManager.RTC_WAKEUP,
                            cube_newtime_a1, PendingIntent.getBroadcast(this, 20,
                                    intent_cube_a1,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                } else {
                    cube_alarmManager_a1.set(AlarmManager.RTC_WAKEUP,
                            cube_newtime_a1, PendingIntent.getBroadcast(this, 20,
                                    intent_cube_a1,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                }

                refreshCUBE(1);
                cube_timerHasStarted_a1 = true;
                break;
            case (R.id.buttona2):
                cube_newtime_a2 = 51000;// 51 seconds
                cube_actualtimea2 = cube_newtime_a2;
                Toast.makeText(
                        getApplicationContext(),
                        (getResources().getString(R.string.game_cube_a2)) + ": "
                                + String.valueOf(cube_newtime_a2 / 1000) + " "
                                + seconds, Toast.LENGTH_SHORT).show();

                // set AlarmManager
                cube_newtime_a2 = new GregorianCalendar().getTimeInMillis()
                        + cube_newtime_a2;
                Intent intent_cube_a2 = new Intent(this, AlarmReciever.class);
                Bundle cube_extras_a2 = new Bundle();
                cube_extras_a2.putString("title",
                        (getResources().getString(R.string.game_cube_a2)));
                cube_extras_a2.putString(
                        "what",
                        the1 + " "
                                + (getResources().getString(R.string.game_cube_a2))
                                + " " + end7);
                intent_cube_a2.putExtras(cube_extras_a2);

                AlarmManager cube_alarmManager_a2 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                if (currentapiVersion >= android.os.Build.VERSION_CODES.KITKAT) {
                    cube_alarmManager_a2.setExact(AlarmManager.RTC_WAKEUP,
                            cube_newtime_a2, PendingIntent.getBroadcast(this, 21,
                                    intent_cube_a2,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                } else {
                    cube_alarmManager_a2.set(AlarmManager.RTC_WAKEUP,
                            cube_newtime_a2, PendingIntent.getBroadcast(this, 21,
                                    intent_cube_a2,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                }

                refreshCUBE(2);
                cube_timerHasStarted_a2 = true;
                break;
            case (R.id.buttona3):
                cube_newtime_a3 = 161000;// 161 seconds
                cube_actualtimea3 = cube_newtime_a3;
                Toast.makeText(
                        getApplicationContext(),
                        (getResources().getString(R.string.game_cube_a3)) + ": "
                                + String.valueOf(cube_newtime_a3 / 1000) + " "
                                + seconds, Toast.LENGTH_SHORT).show();

                // set AlarmManager
                cube_newtime_a3 = new GregorianCalendar().getTimeInMillis()
                        + cube_newtime_a3;
                Intent intent_cube_a3 = new Intent(this, AlarmReciever.class);
                Bundle cube_extras_a3 = new Bundle();
                cube_extras_a3.putString("title",
                        (getResources().getString(R.string.game_cube_a3)));
                cube_extras_a3.putString(
                        "what",
                        the1 + " "
                                + (getResources().getString(R.string.game_cube_a3))
                                + " " + end7);
                intent_cube_a3.putExtras(cube_extras_a3);

                AlarmManager cube_alarmManager_a3 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                if (currentapiVersion >= android.os.Build.VERSION_CODES.KITKAT) {
                    cube_alarmManager_a3.setExact(AlarmManager.RTC_WAKEUP,
                            cube_newtime_a3, PendingIntent.getBroadcast(this, 22,
                                    intent_cube_a3,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                } else {
                    cube_alarmManager_a3.set(AlarmManager.RTC_WAKEUP,
                            cube_newtime_a3, PendingIntent.getBroadcast(this, 22,
                                    intent_cube_a3,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                }

                refreshCUBE(3);
                cube_timerHasStarted_a3 = true;
                break;
            case (R.id.buttonb1):
                cube_newtime_b1 = 25000;// 25 seconds
                cube_actualtimeb1 = cube_newtime_b1;
                Toast.makeText(
                        getApplicationContext(),
                        (getResources().getString(R.string.game_cube_b1)) + ": "
                                + String.valueOf(cube_newtime_b1 / 1000) + " "
                                + seconds, Toast.LENGTH_SHORT).show();

                // set AlarmManager
                cube_newtime_b1 = new GregorianCalendar().getTimeInMillis()
                        + cube_newtime_b1;
                Intent intent_cube_b1 = new Intent(this, AlarmReciever.class);
                Bundle cube_extras_b1 = new Bundle();
                cube_extras_b1.putString("title",
                        (getResources().getString(R.string.game_cube_b1)));
                cube_extras_b1.putString(
                        "what",
                        the1 + " "
                                + (getResources().getString(R.string.game_cube_b1))
                                + " " + end7);
                intent_cube_b1.putExtras(cube_extras_b1);

                AlarmManager cube_alarmManager_b1 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                if (currentapiVersion >= android.os.Build.VERSION_CODES.KITKAT) {
                    cube_alarmManager_b1.setExact(AlarmManager.RTC_WAKEUP,
                            cube_newtime_b1, PendingIntent.getBroadcast(this, 23,
                                    intent_cube_b1,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                } else {
                    cube_alarmManager_b1.set(AlarmManager.RTC_WAKEUP,
                            cube_newtime_b1, PendingIntent.getBroadcast(this, 23,
                                    intent_cube_b1,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                }

                refreshCUBE(4);
                cube_timerHasStarted_b1 = true;
                break;
            case (R.id.buttonb2):
                cube_newtime_b2 = 101000;// 101 seconds
                cube_actualtimeb2 = cube_newtime_b2;
                Toast.makeText(
                        getApplicationContext(),
                        (getResources().getString(R.string.game_cube_b2)) + ": "
                                + String.valueOf(cube_newtime_b2 / 1000) + " "
                                + seconds, Toast.LENGTH_SHORT).show();

                // set AlarmManager
                cube_newtime_b2 = new GregorianCalendar().getTimeInMillis()
                        + cube_newtime_b2;
                Intent intent_cube_b2 = new Intent(this, AlarmReciever.class);
                Bundle cube_extras_b2 = new Bundle();
                cube_extras_b2.putString("title",
                        (getResources().getString(R.string.game_cube_b2)));
                cube_extras_b2.putString(
                        "what",
                        the1 + " "
                                + (getResources().getString(R.string.game_cube_b2))
                                + " " + end7);
                intent_cube_b2.putExtras(cube_extras_b2);

                AlarmManager cube_alarmManager_b2 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                if (currentapiVersion >= android.os.Build.VERSION_CODES.KITKAT) {
                    cube_alarmManager_b2.setExact(AlarmManager.RTC_WAKEUP,
                            cube_newtime_b2, PendingIntent.getBroadcast(this, 24,
                                    intent_cube_b2,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                } else {
                    cube_alarmManager_b2.set(AlarmManager.RTC_WAKEUP,
                            cube_newtime_b2, PendingIntent.getBroadcast(this, 24,
                                    intent_cube_b2,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                }

                refreshCUBE(5);
                cube_timerHasStarted_b2 = true;
                break;
            case (R.id.buttonb3):
                cube_newtime_b3 = 356000;// 356 seconds
                cube_actualtimeb3 = cube_newtime_b3;
                Toast.makeText(
                        getApplicationContext(),
                        (getResources().getString(R.string.game_cube_b3)) + ": "
                                + String.valueOf(cube_newtime_b3 / 1000) + " "
                                + seconds, Toast.LENGTH_SHORT).show();

                // set AlarmManager
                cube_newtime_b3 = new GregorianCalendar().getTimeInMillis()
                        + cube_newtime_b3;
                Intent intent_cube_b3 = new Intent(this, AlarmReciever.class);
                Bundle cube_extras_b3 = new Bundle();
                cube_extras_b3.putString("title",
                        (getResources().getString(R.string.game_cube_b3)));
                cube_extras_b3.putString(
                        "what",
                        the1 + " "
                                + (getResources().getString(R.string.game_cube_b3))
                                + " " + end7);
                intent_cube_b3.putExtras(cube_extras_b3);

                AlarmManager cube_alarmManager_b3 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                if (currentapiVersion >= android.os.Build.VERSION_CODES.KITKAT) {
                    cube_alarmManager_b3.setExact(AlarmManager.RTC_WAKEUP,
                            cube_newtime_b3, PendingIntent.getBroadcast(this, 25,
                                    intent_cube_b3,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                } else {
                    cube_alarmManager_b3.set(AlarmManager.RTC_WAKEUP,
                            cube_newtime_b3, PendingIntent.getBroadcast(this, 25,
                                    intent_cube_b3,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                }

                refreshCUBE(6);
                cube_timerHasStarted_b3 = true;
                break;
        }
    }

    public void gamedota2(View v) {
        dota2_newtime_a1 = 600000;// 600 seconds
        dota2_actualtimea1 = dota2_newtime_a1;
        Toast.makeText(
                getApplicationContext(),
                (getResources().getString(R.string.game_dota2_a1)) + ": "
                        + String.valueOf(dota2_newtime_a1 / 1000 / 60) + " "
                        + minutes, Toast.LENGTH_SHORT).show();

        // set AlarmManager
        dota2_newtime_a1 = new GregorianCalendar().getTimeInMillis()
                + dota2_newtime_a1;
        Intent intent_dota2_a1 = new Intent(this, AlarmReciever.class);
        Bundle dota2_extras_a1 = new Bundle();
        dota2_extras_a1.putString("title",
                (getResources().getString(R.string.game_dota2_a1)));
        dota2_extras_a1.putString("what",
                the1 + " " + (getResources().getString(R.string.game_dota2_a1))
                        + " " + end4);
        intent_dota2_a1.putExtras(dota2_extras_a1);

        AlarmManager dota2_alarmManager_a1 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        if (currentapiVersion >= android.os.Build.VERSION_CODES.KITKAT) {
            dota2_alarmManager_a1
                    .setExact(AlarmManager.RTC_WAKEUP, dota2_newtime_a1,
                            PendingIntent.getBroadcast(this, 14,
                                    intent_dota2_a1,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
        } else {
            dota2_alarmManager_a1
                    .set(AlarmManager.RTC_WAKEUP, dota2_newtime_a1,
                            PendingIntent.getBroadcast(this, 14,
                                    intent_dota2_a1,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
        }

        refreshDota2();
        dota2_timerHasStarted_a1 = true;
    }

    public void gameps2(View v) {
        return;
    }

    public void gamegtav(View v) {
        switch (v.getId()) {
            case (R.id.buttona1):
                gtav_newtime_a1 = 2880000;// 2880 seconds
                gtav_actualtimea1 = gtav_newtime_a1;
                Toast.makeText(
                        getApplicationContext(),
                        (getResources().getString(R.string.game_gtav_a1)) + ": "
                                + String.valueOf(gtav_newtime_a1 / 1000 / 60) + " "
                                + minutes, Toast.LENGTH_SHORT).show();

                // set AlarmManager
                gtav_newtime_a1 = new GregorianCalendar().getTimeInMillis()
                        + gtav_newtime_a1;
                Intent intent_gtav_a1 = new Intent(this, AlarmReciever.class);
                Bundle gtav_extras_a1 = new Bundle();
                gtav_extras_a1.putString("title",
                        (getResources().getString(R.string.game_gtav_a1)));
                gtav_extras_a1.putString(
                        "what",
                        the1 + " "
                                + (getResources().getString(R.string.game_gtav_a1))
                                + " " + end2);
                intent_gtav_a1.putExtras(gtav_extras_a1);

                AlarmManager gtav_alarmManager_a1 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                if (currentapiVersion >= android.os.Build.VERSION_CODES.KITKAT) {
                    gtav_alarmManager_a1.setExact(AlarmManager.RTC_WAKEUP,
                            gtav_newtime_a1, PendingIntent.getBroadcast(this, 11,
                                    intent_gtav_a1,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                } else {
                    gtav_alarmManager_a1.set(AlarmManager.RTC_WAKEUP,
                            gtav_newtime_a1, PendingIntent.getBroadcast(this, 11,
                                    intent_gtav_a1,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                }

                refreshGTAV(1);
                gtav_timerHasStarted_a1 = true;
                break;
            case (R.id.buttona2):
                gtav_newtime_a2 = 120000;// 120 seconds
                gtav_actualtimea2 = gtav_newtime_a2;
                Toast.makeText(
                        getApplicationContext(),
                        (getResources().getString(R.string.game_gtav_a2)) + ": "
                                + String.valueOf(gtav_newtime_a2 / 1000 / 60) + " "
                                + minutes, Toast.LENGTH_SHORT).show();

                // set AlarmManager
                gtav_newtime_a2 = new GregorianCalendar().getTimeInMillis()
                        + gtav_newtime_a2;
                Intent intent_gtav_a2 = new Intent(this, AlarmReciever.class);
                Bundle gtav_extras_a2 = new Bundle();
                gtav_extras_a2.putString("title",
                        (getResources().getString(R.string.game_gtav_a2)));
                gtav_extras_a2.putString(
                        "what",
                        the1 + " "
                                + (getResources().getString(R.string.game_gtav_a2))
                                + " " + end3);
                intent_gtav_a2.putExtras(gtav_extras_a2);

                AlarmManager gtav_alarmManager_a2 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                if (currentapiVersion >= android.os.Build.VERSION_CODES.KITKAT) {
                    gtav_alarmManager_a2.setExact(AlarmManager.RTC_WAKEUP,
                            gtav_newtime_a2, PendingIntent.getBroadcast(this, 12,
                                    intent_gtav_a2,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                } else {
                    gtav_alarmManager_a2.set(AlarmManager.RTC_WAKEUP,
                            gtav_newtime_a2, PendingIntent.getBroadcast(this, 12,
                                    intent_gtav_a2,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                }

                refreshGTAV(2);
                gtav_timerHasStarted_a2 = true;
                break;
            case (R.id.buttona3):
                gtav_newtime_a3 = 150000;// 150 seconds
                gtav_actualtimea3 = gtav_newtime_a3;
                Toast.makeText(
                        getApplicationContext(),
                        (getResources().getString(R.string.game_gtav_a3)) + ": "
                                + String.valueOf(gtav_newtime_a3 / 1000 / 60) + " "
                                + minutes, Toast.LENGTH_SHORT).show();

                // set AlarmManager
                gtav_newtime_a3 = new GregorianCalendar().getTimeInMillis()
                        + gtav_newtime_a3;
                Intent intent_gtav_a3 = new Intent(this, AlarmReciever.class);
                Bundle gtav_extras_a3 = new Bundle();
                gtav_extras_a3.putString("title",
                        (getResources().getString(R.string.game_gtav_a3)));
                gtav_extras_a3.putString(
                        "what",
                        the1 + " "
                                + (getResources().getString(R.string.game_gtav_a3))
                                + " " + end4);
                intent_gtav_a3.putExtras(gtav_extras_a3);

                AlarmManager gtav_alarmManager_a3 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                if (currentapiVersion >= android.os.Build.VERSION_CODES.KITKAT) {
                    gtav_alarmManager_a3.setExact(AlarmManager.RTC_WAKEUP,
                            gtav_newtime_a3, PendingIntent.getBroadcast(this, 13,
                                    intent_gtav_a3,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                } else {
                    gtav_alarmManager_a3.set(AlarmManager.RTC_WAKEUP,
                            gtav_newtime_a3, PendingIntent.getBroadcast(this, 13,
                                    intent_gtav_a3,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                }

                refreshGTAV(3);
                gtav_timerHasStarted_a3 = true;
                break;
        }
    }

    public void gamelol(View v) {
        switch (v.getId()) {
            case (R.id.buttona1):
                lol_newtime_a1 = 300000;// 300 seconds
                lol_actualtimea1 = lol_newtime_a1;
                Toast.makeText(
                        getApplicationContext(),
                        (getResources().getString(R.string.game_lol_a1)) + ": "
                                + String.valueOf(lol_newtime_a1 / 1000 / 60) + " "
                                + minutes, Toast.LENGTH_SHORT).show();

                // set AlarmManager
                lol_newtime_a1 = new GregorianCalendar().getTimeInMillis()
                        + lol_newtime_a1;
                Intent intent_lol_a1 = new Intent(this, AlarmReciever.class);
                Bundle lol_extras_a1 = new Bundle();
                lol_extras_a1.putString("title",
                        (getResources().getString(R.string.game_lol_a1)));
                lol_extras_a1.putString(
                        "what",
                        the1 + " "
                                + (getResources().getString(R.string.game_lol_a1))
                                + " " + end4);
                intent_lol_a1.putExtras(lol_extras_a1);

                AlarmManager lol_alarmManager_a1 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                if (currentapiVersion >= android.os.Build.VERSION_CODES.KITKAT) {
                    lol_alarmManager_a1.setExact(AlarmManager.RTC_WAKEUP,
                            lol_newtime_a1, PendingIntent.getBroadcast(this, 1,
                                    intent_lol_a1,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                } else {
                    lol_alarmManager_a1.set(AlarmManager.RTC_WAKEUP,
                            lol_newtime_a1, PendingIntent.getBroadcast(this, 1,
                                    intent_lol_a1,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                }

                refreshLoL(1);
                lol_timerHasStarted_a1 = true;
                break;

            case (R.id.buttona2):
                lol_newtime_a2 = 360000;// 360 seconds
                lol_actualtimea2 = lol_newtime_a2;
                Toast.makeText(
                        getApplicationContext(),
                        (getResources().getString(R.string.game_lol_a2)) + ": "
                                + String.valueOf(lol_newtime_a2 / 1000 / 60) + " "
                                + minutes, Toast.LENGTH_SHORT).show();

                // set AlarmManager
                lol_newtime_a2 = new GregorianCalendar().getTimeInMillis()
                        + lol_newtime_a2;
                Intent intent_lol_a2 = new Intent(this, AlarmReciever.class);
                Bundle lol_extras_a2 = new Bundle();
                lol_extras_a2.putString("title",
                        (getResources().getString(R.string.game_lol_a2)));
                lol_extras_a2.putString(
                        "what",
                        the1 + " "
                                + (getResources().getString(R.string.game_lol_a2))
                                + " " + end4);
                intent_lol_a2.putExtras(lol_extras_a2);

                AlarmManager lol_alarmManager_a2 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                if (currentapiVersion >= android.os.Build.VERSION_CODES.KITKAT) {
                    lol_alarmManager_a2.setExact(AlarmManager.RTC_WAKEUP,
                            lol_newtime_a2, PendingIntent.getBroadcast(this, 2,
                                    intent_lol_a2,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                } else {
                    lol_alarmManager_a2.set(AlarmManager.RTC_WAKEUP,
                            lol_newtime_a2, PendingIntent.getBroadcast(this, 2,
                                    intent_lol_a2,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                }

                refreshLoL(2);
                lol_timerHasStarted_a2 = true;
                break;

            case (R.id.buttona3):
                lol_newtime_a3 = 180000;// 180 seconds
                lol_actualtimea3 = lol_newtime_a3;
                Toast.makeText(
                        getApplicationContext(),
                        (getResources().getString(R.string.game_lol_a3)) + ": "
                                + String.valueOf(lol_newtime_a3 / 1000 / 60) + " "
                                + minutes, Toast.LENGTH_SHORT).show();

                // set AlarmManager
                lol_newtime_a3 = new GregorianCalendar().getTimeInMillis()
                        + lol_newtime_a3;
                Intent intent_lol_a3 = new Intent(this, AlarmReciever.class);
                Bundle lol_extras_a3 = new Bundle();
                lol_extras_a3.putString("title",
                        (getResources().getString(R.string.game_lol_a3)));
                lol_extras_a3.putString(
                        "what",
                        the1 + " "
                                + (getResources().getString(R.string.game_lol_a3))
                                + " " + end4);
                intent_lol_a3.putExtras(lol_extras_a3);

                AlarmManager lol_alarmManager_a3 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                if (currentapiVersion >= android.os.Build.VERSION_CODES.KITKAT) {
                    lol_alarmManager_a3.setExact(AlarmManager.RTC_WAKEUP,
                            lol_newtime_a3, PendingIntent.getBroadcast(this, 3,
                                    intent_lol_a3,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                } else {
                    lol_alarmManager_a3.set(AlarmManager.RTC_WAKEUP,
                            lol_newtime_a3, PendingIntent.getBroadcast(this, 3,
                                    intent_lol_a3,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                }

                refreshLoL(3);
                lol_timerHasStarted_a3 = true;
                break;

            case (R.id.buttona4):
                lol_newtime_a4 = 50000;// 50 seconds
                lol_actualtimea4 = lol_newtime_a4;
                Toast.makeText(
                        getApplicationContext(),
                        (getResources().getString(R.string.game_lol_a4)) + ": "
                                + String.valueOf(lol_newtime_a4 / 1000) + " "
                                + seconds, Toast.LENGTH_SHORT).show();

                // set AlarmManager
                lol_newtime_a4 = new GregorianCalendar().getTimeInMillis()
                        + lol_newtime_a4;
                Intent intent_lol_a4 = new Intent(this, AlarmReciever.class);
                Bundle lol_extras_a4 = new Bundle();
                lol_extras_a4.putString("title",
                        (getResources().getString(R.string.game_lol_a4)));
                lol_extras_a4.putString(
                        "what",
                        the2 + " "
                                + (getResources().getString(R.string.game_lol_a4))
                                + " " + end5);
                intent_lol_a4.putExtras(lol_extras_a4);

                AlarmManager lol_alarmManager_a4 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                if (currentapiVersion >= android.os.Build.VERSION_CODES.KITKAT) {
                    lol_alarmManager_a4.setExact(AlarmManager.RTC_WAKEUP,
                            lol_newtime_a4, PendingIntent.getBroadcast(this, 4,
                                    intent_lol_a4,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                } else {
                    lol_alarmManager_a4.set(AlarmManager.RTC_WAKEUP,
                            lol_newtime_a4, PendingIntent.getBroadcast(this, 4,
                                    intent_lol_a4,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                }

                refreshLoL(4);
                lol_timerHasStarted_a4 = true;
                break;

            case (R.id.buttona5):
                lol_newtime_a5 = 50000;// 50 seconds
                lol_actualtimea5 = lol_newtime_a5;
                Toast.makeText(
                        getApplicationContext(),
                        (getResources().getString(R.string.game_lol_a5)) + ": "
                                + String.valueOf(lol_newtime_a5 / 1000) + " "
                                + seconds, Toast.LENGTH_SHORT).show();

                // set AlarmManager
                lol_newtime_a5 = new GregorianCalendar().getTimeInMillis()
                        + lol_newtime_a5;
                Intent intent_lol_a5 = new Intent(this, AlarmReciever.class);
                Bundle lol_extras_a5 = new Bundle();
                lol_extras_a5.putString("title",
                        (getResources().getString(R.string.game_lol_a5)));
                lol_extras_a5.putString(
                        "what",
                        the1 + " "
                                + (getResources().getString(R.string.game_lol_a5))
                                + " " + end4);
                intent_lol_a5.putExtras(lol_extras_a5);

                AlarmManager lol_alarmManager_a5 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                if (currentapiVersion >= android.os.Build.VERSION_CODES.KITKAT) {
                    lol_alarmManager_a5.setExact(AlarmManager.RTC_WAKEUP,
                            lol_newtime_a5, PendingIntent.getBroadcast(this, 5,
                                    intent_lol_a5,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                } else {
                    lol_alarmManager_a5.set(AlarmManager.RTC_WAKEUP,
                            lol_newtime_a5, PendingIntent.getBroadcast(this, 5,
                                    intent_lol_a5,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                }

                refreshLoL(5);
                lol_timerHasStarted_a5 = true;
                break;

            case (R.id.buttonb1):
                lol_newtime_b1 = 300000;// 300 seconds
                lol_actualtimeb1 = lol_newtime_b1;
                Toast.makeText(
                        getApplicationContext(),
                        (getResources().getString(R.string.game_lol_b1)) + ": "
                                + String.valueOf(lol_newtime_b1 / 1000 / 60) + " "
                                + minutes, Toast.LENGTH_SHORT).show();

                // set AlarmManager
                lol_newtime_b1 = new GregorianCalendar().getTimeInMillis()
                        + lol_newtime_b1;
                Intent intent_lol_b1 = new Intent(this, AlarmReciever.class);
                Bundle lol_extras_b1 = new Bundle();
                lol_extras_b1.putString("title",
                        (getResources().getString(R.string.game_lol_b1)));
                lol_extras_b1.putString(
                        "what",
                        the1 + " "
                                + (getResources().getString(R.string.game_lol_b1))
                                + " " + end4);
                intent_lol_b1.putExtras(lol_extras_b1);

                AlarmManager lol_alarmManager_b1 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                if (currentapiVersion >= android.os.Build.VERSION_CODES.KITKAT) {
                    lol_alarmManager_b1.setExact(AlarmManager.RTC_WAKEUP,
                            lol_newtime_b1, PendingIntent.getBroadcast(this, 6,
                                    intent_lol_b1,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                } else {
                    lol_alarmManager_b1.set(AlarmManager.RTC_WAKEUP,
                            lol_newtime_b1, PendingIntent.getBroadcast(this, 6,
                                    intent_lol_b1,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                }

                refreshLoL(6);
                lol_timerHasStarted_b1 = true;
                break;

            case (R.id.buttonb2):
                lol_newtime_b2 = 420000;// 420 seconds
                lol_actualtimeb2 = lol_newtime_b2;
                Toast.makeText(
                        getApplicationContext(),
                        (getResources().getString(R.string.game_lol_b2)) + ": "
                                + String.valueOf(lol_newtime_b2 / 1000 / 60) + " "
                                + minutes, Toast.LENGTH_SHORT).show();

                // set AlarmManager
                lol_newtime_b2 = new GregorianCalendar().getTimeInMillis()
                        + lol_newtime_b2;
                Intent intent_lol_b2 = new Intent(this, AlarmReciever.class);
                Bundle lol_extras_b2 = new Bundle();
                lol_extras_b2.putString("title",
                        (getResources().getString(R.string.game_lol_b2)));
                lol_extras_b2.putString(
                        "what",
                        the1 + " "
                                + (getResources().getString(R.string.game_lol_b2))
                                + " " + end4);
                intent_lol_b2.putExtras(lol_extras_b2);

                AlarmManager lol_alarmManager_b2 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                if (currentapiVersion >= android.os.Build.VERSION_CODES.KITKAT) {
                    lol_alarmManager_b2.setExact(AlarmManager.RTC_WAKEUP,
                            lol_newtime_b2, PendingIntent.getBroadcast(this, 7,
                                    intent_lol_b2,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                } else {
                    lol_alarmManager_b2.set(AlarmManager.RTC_WAKEUP,
                            lol_newtime_b2, PendingIntent.getBroadcast(this, 7,
                                    intent_lol_b2,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                }

                refreshLoL(7);
                lol_timerHasStarted_b2 = true;
                break;

            case (R.id.buttonb3):
                lol_newtime_b3 = 180000;// 180 seconds
                lol_actualtimeb3 = lol_newtime_b3;
                Toast.makeText(
                        getApplicationContext(),
                        (getResources().getString(R.string.game_lol_b3)) + ": "
                                + String.valueOf(lol_newtime_b3 / 1000 / 60) + " "
                                + minutes, Toast.LENGTH_SHORT).show();

                // set AlarmManager
                lol_newtime_b3 = new GregorianCalendar().getTimeInMillis()
                        + lol_newtime_b3;
                Intent intent_lol_b3 = new Intent(this, AlarmReciever.class);
                Bundle lol_extras_b3 = new Bundle();
                lol_extras_b3.putString("title",
                        (getResources().getString(R.string.game_lol_b3)));
                lol_extras_b3.putString(
                        "what",
                        the1 + " "
                                + (getResources().getString(R.string.game_lol_b3))
                                + " " + end4);
                intent_lol_b3.putExtras(lol_extras_b3);

                AlarmManager lol_alarmManager_b3 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                if (currentapiVersion >= android.os.Build.VERSION_CODES.KITKAT) {
                    lol_alarmManager_b3.setExact(AlarmManager.RTC_WAKEUP,
                            lol_newtime_b3, PendingIntent.getBroadcast(this, 8,
                                    intent_lol_b3,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                } else {
                    lol_alarmManager_b3.set(AlarmManager.RTC_WAKEUP,
                            lol_newtime_b3, PendingIntent.getBroadcast(this, 8,
                                    intent_lol_b3,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                }

                refreshLoL(8);
                lol_timerHasStarted_b3 = true;
                break;

            case (R.id.buttonb4):
                lol_newtime_b4 = 50000;// 50 seconds
                lol_actualtimeb4 = lol_newtime_b4;
                Toast.makeText(
                        getApplicationContext(),
                        (getResources().getString(R.string.game_lol_b4)) + ": "
                                + String.valueOf(lol_newtime_b4 / 1000) + " "
                                + seconds, Toast.LENGTH_SHORT).show();

                // set AlarmManager
                lol_newtime_b4 = new GregorianCalendar().getTimeInMillis()
                        + lol_newtime_b4;
                Intent intent_lol_b4 = new Intent(this, AlarmReciever.class);
                Bundle lol_extras_b4 = new Bundle();
                lol_extras_b4.putString("title",
                        (getResources().getString(R.string.game_lol_b4)));
                lol_extras_b4.putString(
                        "what",
                        the2 + " "
                                + (getResources().getString(R.string.game_lol_b4))
                                + " " + end5);
                intent_lol_b4.putExtras(lol_extras_b4);

                AlarmManager lol_alarmManager_b4 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

                if (currentapiVersion >= android.os.Build.VERSION_CODES.KITKAT) {
                    lol_alarmManager_b4.setExact(AlarmManager.RTC_WAKEUP,
                            lol_newtime_b4, PendingIntent.getBroadcast(this, 9,
                                    intent_lol_b4,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                } else {
                    lol_alarmManager_b4.set(AlarmManager.RTC_WAKEUP,
                            lol_newtime_b4, PendingIntent.getBroadcast(this, 9,
                                    intent_lol_b4,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                }
                refreshLoL(9);
                lol_timerHasStarted_b4 = true;
                break;

            case (R.id.buttonb5):
                lol_newtime_b5 = 50000;// 50 seconds
                lol_actualtimeb5 = lol_newtime_b5;
                Toast.makeText(
                        getApplicationContext(),
                        (getResources().getString(R.string.game_lol_b5)) + ": "
                                + String.valueOf(lol_newtime_b5 / 1000) + " "
                                + seconds, Toast.LENGTH_SHORT).show();

                // set AlarmManager
                lol_newtime_b5 = new GregorianCalendar().getTimeInMillis()
                        + lol_newtime_b5;
                Intent intent_lol_b5 = new Intent(this, AlarmReciever.class);
                Bundle lol_extras_b5 = new Bundle();
                lol_extras_b5.putString("title",
                        (getResources().getString(R.string.game_lol_b5)));
                lol_extras_b5.putString(
                        "what",
                        the2 + " "
                                + (getResources().getString(R.string.game_lol_b5))
                                + " " + end5);
                intent_lol_b5.putExtras(lol_extras_b5);

                AlarmManager lol_alarmManager_b5 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

                if (currentapiVersion >= android.os.Build.VERSION_CODES.KITKAT) {
                    lol_alarmManager_b5.setExact(AlarmManager.RTC_WAKEUP,
                            lol_newtime_b5, PendingIntent.getBroadcast(this, 10,
                                    intent_lol_b5,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                } else {
                    lol_alarmManager_b5.set(AlarmManager.RTC_WAKEUP,
                            lol_newtime_b5, PendingIntent.getBroadcast(this, 10,
                                    intent_lol_b5,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                }
                refreshLoL(10);
                lol_timerHasStarted_b5 = true;
                break;
        }
    }

    public void gamesc2(View v) {
        switch (v.getId()) {
            case (R.id.buttona1):
                sc2_newtime_a1 = 13000;// 13 seconds
                sc2_actualtimea1 = sc2_newtime_a1;
                Toast.makeText(
                        getApplicationContext(),
                        (getResources().getString(R.string.game_sc2_a1)) + ": "
                                + String.valueOf(sc2_newtime_a1 / 1000) + " "
                                + seconds, Toast.LENGTH_SHORT).show();

                // set AlarmManager
                sc2_newtime_a1 = new GregorianCalendar().getTimeInMillis()
                        + sc2_newtime_a1;
                Intent intent_sc2_a1 = new Intent(this, AlarmReciever.class);
                Bundle sc2_extras_a1 = new Bundle();
                sc2_extras_a1.putString("title",
                        (getResources().getString(R.string.game_sc2_a1)));
                sc2_extras_a1.putString(
                        "what",
                        the1 + " "
                                + (getResources().getString(R.string.game_sc2_a1))
                                + " " + end6);
                intent_sc2_a1.putExtras(sc2_extras_a1);

                AlarmManager sc2_alarmManager_a1 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                if (currentapiVersion >= android.os.Build.VERSION_CODES.KITKAT) {
                    sc2_alarmManager_a1.setExact(AlarmManager.RTC_WAKEUP,
                            sc2_newtime_a1, PendingIntent.getBroadcast(this, 15,
                                    intent_sc2_a1,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                } else {
                    sc2_alarmManager_a1.set(AlarmManager.RTC_WAKEUP,
                            sc2_newtime_a1, PendingIntent.getBroadcast(this, 15,
                                    intent_sc2_a1,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                }

                refreshSC2(1);
                sc2_timerHasStarted_a1 = true;
                break;
            case (R.id.buttona2):
                sc2_newtime_a2 = 60000;// 60 seconds
                sc2_actualtimea2 = sc2_newtime_a2;
                Toast.makeText(
                        getApplicationContext(),
                        (getResources().getString(R.string.game_sc2_a2)) + ": "
                                + String.valueOf(sc2_newtime_a2 / 1000 / 60) + " "
                                + minute, Toast.LENGTH_SHORT).show();

                // set AlarmManager
                sc2_newtime_a2 = new GregorianCalendar().getTimeInMillis()
                        + sc2_newtime_a2;
                Intent intent_sc2_a2 = new Intent(this, AlarmReciever.class);
                Bundle sc2_extras_a2 = new Bundle();
                sc2_extras_a2.putString("title",
                        (getResources().getString(R.string.game_sc2_a2)));
                sc2_extras_a2.putString(
                        "what",
                        the1 + " "
                                + (getResources().getString(R.string.game_sc2_a2))
                                + " " + end6);
                intent_sc2_a2.putExtras(sc2_extras_a2);

                AlarmManager sc2_alarmManager_a2 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                if (currentapiVersion >= android.os.Build.VERSION_CODES.KITKAT) {
                    sc2_alarmManager_a2.setExact(AlarmManager.RTC_WAKEUP,
                            sc2_newtime_a2, PendingIntent.getBroadcast(this, 16,
                                    intent_sc2_a2,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                } else {
                    sc2_alarmManager_a2.set(AlarmManager.RTC_WAKEUP,
                            sc2_newtime_a2, PendingIntent.getBroadcast(this, 16,
                                    intent_sc2_a2,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                }

                refreshSC2(2);
                sc2_timerHasStarted_a2 = true;
                break;
            case (R.id.buttona3):
                sc2_newtime_a3 = 150000;// 150 seconds
                sc2_actualtimea3 = sc2_newtime_a3;
                Toast.makeText(
                        getApplicationContext(),
                        (getResources().getString(R.string.game_sc2_a3)) + ": "
                                + String.valueOf(sc2_newtime_a3 / 1000) + " "
                                + seconds, Toast.LENGTH_SHORT).show();

                // set AlarmManager
                sc2_newtime_a3 = new GregorianCalendar().getTimeInMillis()
                        + sc2_newtime_a3;
                Intent intent_sc2_a3 = new Intent(this, AlarmReciever.class);
                Bundle sc2_extras_a3 = new Bundle();
                sc2_extras_a3.putString("title",
                        (getResources().getString(R.string.game_sc2_a3)));
                sc2_extras_a3.putString(
                        "what",
                        the1 + " "
                                + (getResources().getString(R.string.game_sc2_a3))
                                + " " + end6);
                intent_sc2_a3.putExtras(sc2_extras_a3);

                AlarmManager sc2_alarmManager_a3 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                if (currentapiVersion >= android.os.Build.VERSION_CODES.KITKAT) {
                    sc2_alarmManager_a3.setExact(AlarmManager.RTC_WAKEUP,
                            sc2_newtime_a3, PendingIntent.getBroadcast(this, 17,
                                    intent_sc2_a3,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                } else {
                    sc2_alarmManager_a3.set(AlarmManager.RTC_WAKEUP,
                            sc2_newtime_a3, PendingIntent.getBroadcast(this, 17,
                                    intent_sc2_a3,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                }

                refreshSC2(3);
                sc2_timerHasStarted_a3 = true;
                break;
            case (R.id.buttona4):
                sc2_newtime_a4 = 150000;// 150 seconds
                sc2_actualtimea4 = sc2_newtime_a4;
                Toast.makeText(
                        getApplicationContext(),
                        (getResources().getString(R.string.game_sc2_a4)) + ": "
                                + String.valueOf(sc2_newtime_a4 / 1000) + " "
                                + seconds, Toast.LENGTH_SHORT).show();

                // set AlarmManager
                sc2_newtime_a4 = new GregorianCalendar().getTimeInMillis()
                        + sc2_newtime_a4;
                Intent intent_sc2_a4 = new Intent(this, AlarmReciever.class);
                Bundle sc2_extras_a4 = new Bundle();
                sc2_extras_a4.putString("title",
                        (getResources().getString(R.string.game_sc2_a4)));
                sc2_extras_a4.putString(
                        "what",
                        the1 + " "
                                + (getResources().getString(R.string.game_sc2_a4))
                                + " " + end6);
                intent_sc2_a4.putExtras(sc2_extras_a4);

                AlarmManager sc2_alarmManager_a4 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                if (currentapiVersion >= android.os.Build.VERSION_CODES.KITKAT) {
                    sc2_alarmManager_a4.setExact(AlarmManager.RTC_WAKEUP,
                            sc2_newtime_a4, PendingIntent.getBroadcast(this, 18,
                                    intent_sc2_a4,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                } else {
                    sc2_alarmManager_a4.set(AlarmManager.RTC_WAKEUP,
                            sc2_newtime_a4, PendingIntent.getBroadcast(this, 18,
                                    intent_sc2_a4,
                                    PendingIntent.FLAG_UPDATE_CURRENT));
                }

                refreshSC2(4);
                sc2_timerHasStarted_a4 = true;
                break;
        }
    }

    public void refreshLoL(int LoLID) {
        switch (LoLID) {
            case (1):
                final Button buttona1 = (Button) findViewById(R.id.buttona1);
                Thread lol_a1 = new Thread() {

                    @Override
                    public void run() {
                        try {
                            while (lol_actualtimea1 >= 1) {
                                Thread.sleep(delay);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        lol_actualtimea1 = (1000 + lol_newtime_a1 - new GregorianCalendar()
                                                .getTimeInMillis()) / 1000;
                                        buttona1.setText((getResources()
                                                .getString(R.string.game_lol_a1))
                                                + ": "
                                                + String.valueOf(lol_actualtimea1));
                                        lol_timerHasStarted_a1 = true;

                                        if (lol_actualtimea1 <= 0) {
                                            buttona1.setText((getResources()
                                                    .getString(R.string.game_lol_a1))
                                                    + " (End)");
                                            lol_timerHasStarted_a1 = false;
                                        }
                                    }
                                });
                            }
                        } catch (InterruptedException e) {
                        }
                    }
                };
                lol_a1.start();
                break;

            case (2):
                final Button buttona2 = (Button) findViewById(R.id.buttona2);
                Thread lol_a2 = new Thread() {

                    @Override
                    public void run() {
                        try {
                            while (lol_actualtimea2 >= 1) {
                                Thread.sleep(delay);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        lol_actualtimea2 = (1000 + lol_newtime_a2 - new GregorianCalendar()
                                                .getTimeInMillis()) / 1000;
                                        buttona2.setText((getResources()
                                                .getString(R.string.game_lol_a2))
                                                + ": "
                                                + String.valueOf(lol_actualtimea2));
                                        lol_timerHasStarted_a2 = true;

                                        if (lol_actualtimea2 <= 0) {
                                            buttona2.setText((getResources()
                                                    .getString(R.string.game_lol_a2))
                                                    + " (End)");
                                            lol_timerHasStarted_a2 = false;
                                        }
                                    }
                                });
                            }
                        } catch (InterruptedException e) {
                        }
                    }
                };
                lol_a2.start();
                break;

            case (3):
                final Button buttona3 = (Button) findViewById(R.id.buttona3);
                Thread lol_a3 = new Thread() {

                    @Override
                    public void run() {
                        try {
                            while (lol_actualtimea3 >= 1) {
                                Thread.sleep(delay);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        lol_actualtimea3 = (1000 + lol_newtime_a3 - new GregorianCalendar()
                                                .getTimeInMillis()) / 1000;
                                        buttona3.setText((getResources()
                                                .getString(R.string.game_lol_a3))
                                                + ": "
                                                + String.valueOf(lol_actualtimea3));
                                        lol_timerHasStarted_a3 = true;

                                        if (lol_actualtimea3 <= 0) {
                                            buttona3.setText((getResources()
                                                    .getString(R.string.game_lol_a3))
                                                    + " (End)");
                                            lol_timerHasStarted_a3 = false;
                                        }
                                    }
                                });
                            }
                        } catch (InterruptedException e) {
                        }
                    }
                };
                lol_a3.start();
                break;

            case (4):
                final Button buttona4 = (Button) findViewById(R.id.buttona4);
                Thread lol_a4 = new Thread() {

                    @Override
                    public void run() {
                        try {
                            while (lol_actualtimea4 >= 1) {
                                Thread.sleep(delay);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        lol_actualtimea4 = (1000 + lol_newtime_a4 - new GregorianCalendar()
                                                .getTimeInMillis()) / 1000;
                                        buttona4.setText((getResources()
                                                .getString(R.string.game_lol_a4))
                                                + ": "
                                                + String.valueOf(lol_actualtimea4));
                                        lol_timerHasStarted_a4 = true;

                                        if (lol_actualtimea4 <= 0) {
                                            buttona4.setText((getResources()
                                                    .getString(R.string.game_lol_a4))
                                                    + " (End)");
                                            lol_timerHasStarted_a4 = false;
                                        }
                                    }
                                });
                            }
                        } catch (InterruptedException e) {
                        }
                    }
                };
                lol_a4.start();
                break;

            case (5):
                final Button buttona5 = (Button) findViewById(R.id.buttona5);
                Thread lol_a5 = new Thread() {

                    @Override
                    public void run() {
                        try {
                            while (lol_actualtimea5 >= 1) {
                                Thread.sleep(delay);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        lol_actualtimea5 = (1000 + lol_newtime_a5 - new GregorianCalendar()
                                                .getTimeInMillis()) / 1000;
                                        buttona5.setText((getResources()
                                                .getString(R.string.game_lol_a5))
                                                + ": "
                                                + String.valueOf(lol_actualtimea5));
                                        lol_timerHasStarted_a5 = true;

                                        if (lol_actualtimea5 <= 0) {
                                            buttona5.setText((getResources()
                                                    .getString(R.string.game_lol_a5))
                                                    + " (End)");
                                            lol_timerHasStarted_a5 = false;
                                        }
                                    }
                                });
                            }
                        } catch (InterruptedException e) {
                        }
                    }
                };
                lol_a5.start();
                break;

            case (6):
                final Button buttonb1 = (Button) findViewById(R.id.buttonb1);
                Thread lol_b1 = new Thread() {

                    @Override
                    public void run() {
                        try {
                            while (lol_actualtimeb1 >= 1) {
                                Thread.sleep(delay);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        lol_actualtimeb1 = (1000 + lol_newtime_b1 - new GregorianCalendar()
                                                .getTimeInMillis()) / 1000;
                                        buttonb1.setText((getResources()
                                                .getString(R.string.game_lol_b1))
                                                + ": "
                                                + String.valueOf(lol_actualtimeb1));
                                        lol_timerHasStarted_b1 = true;

                                        if (lol_actualtimeb1 <= 0) {
                                            buttonb1.setText((getResources()
                                                    .getString(R.string.game_lol_b1))
                                                    + " (End)");
                                            lol_timerHasStarted_b1 = false;
                                        }
                                    }
                                });
                            }
                        } catch (InterruptedException e) {
                        }
                    }
                };
                lol_b1.start();
                break;

            case (7):
                final Button buttonb2 = (Button) findViewById(R.id.buttonb2);
                Thread lol_b2 = new Thread() {

                    @Override
                    public void run() {
                        try {
                            while (lol_actualtimeb2 >= 1) {
                                Thread.sleep(delay);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        lol_actualtimeb2 = (1000 + lol_newtime_b2 - new GregorianCalendar()
                                                .getTimeInMillis()) / 1000;
                                        buttonb2.setText((getResources()
                                                .getString(R.string.game_lol_b2))
                                                + ": "
                                                + String.valueOf(lol_actualtimeb2));
                                        lol_timerHasStarted_b2 = true;

                                        if (lol_actualtimeb2 <= 0) {
                                            buttonb2.setText((getResources()
                                                    .getString(R.string.game_lol_b2))
                                                    + " (End)");
                                            lol_timerHasStarted_b2 = false;
                                        }
                                    }
                                });
                            }
                        } catch (InterruptedException e) {
                        }
                    }
                };
                lol_b2.start();
                break;

            case (8):
                final Button buttonb3 = (Button) findViewById(R.id.buttonb3);
                Thread lol_b3 = new Thread() {

                    @Override
                    public void run() {
                        try {
                            while (lol_actualtimeb3 >= 1) {
                                Thread.sleep(delay);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        lol_actualtimeb3 = (1000 + lol_newtime_b3 - new GregorianCalendar()
                                                .getTimeInMillis()) / 1000;
                                        buttonb3.setText((getResources()
                                                .getString(R.string.game_lol_b3))
                                                + ": "
                                                + String.valueOf(lol_actualtimeb3));
                                        lol_timerHasStarted_b3 = true;

                                        if (lol_actualtimeb3 <= 0) {
                                            buttonb3.setText((getResources()
                                                    .getString(R.string.game_lol_b3))
                                                    + " (End)");
                                            lol_timerHasStarted_b3 = false;
                                        }
                                    }
                                });
                            }
                        } catch (InterruptedException e) {
                        }
                    }
                };
                lol_b3.start();
                break;

            case (9):
                final Button buttonb4 = (Button) findViewById(R.id.buttonb4);
                Thread lol_b4 = new Thread() {

                    @Override
                    public void run() {
                        try {
                            while (lol_actualtimeb4 >= 1) {
                                Thread.sleep(delay);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        lol_actualtimeb4 = (1000 + lol_newtime_b4 - new GregorianCalendar()
                                                .getTimeInMillis()) / 1000;
                                        buttonb4.setText((getResources()
                                                .getString(R.string.game_lol_b4))
                                                + ": "
                                                + String.valueOf(lol_actualtimeb4));
                                        lol_timerHasStarted_b4 = true;

                                        if (lol_actualtimeb4 <= 0) {
                                            buttonb4.setText((getResources()
                                                    .getString(R.string.game_lol_b4))
                                                    + " (End)");
                                            lol_timerHasStarted_b4 = false;
                                        }
                                    }
                                });
                            }
                        } catch (InterruptedException e) {
                        }
                    }
                };
                lol_b4.start();
                break;

            case (10):
                final Button buttonb5 = (Button) findViewById(R.id.buttonb5);
                Thread lol_b5 = new Thread() {

                    @Override
                    public void run() {
                        try {
                            while (lol_actualtimeb5 >= 1) {
                                Thread.sleep(delay);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        lol_actualtimeb5 = (1000 + lol_newtime_b5 - new GregorianCalendar()
                                                .getTimeInMillis()) / 1000;
                                        buttonb5.setText((getResources()
                                                .getString(R.string.game_lol_b5))
                                                + ": "
                                                + String.valueOf(lol_actualtimeb5));
                                        lol_timerHasStarted_b5 = true;

                                        if (lol_actualtimeb5 <= 0) {
                                            buttonb5.setText((getResources()
                                                    .getString(R.string.game_lol_b5))
                                                    + " (End)");
                                            lol_timerHasStarted_b5 = false;
                                        }
                                    }
                                });
                            }
                        } catch (InterruptedException e) {
                        }
                    }
                };
                lol_b5.start();
                break;
        }
    }

    public void refreshGTAV(int GTAVID) {
        switch (GTAVID) {
            case (1):
                final Button buttona1 = (Button) findViewById(R.id.buttona1);
                Thread gtav_a1 = new Thread() {

                    @Override
                    public void run() {
                        try {
                            while (gtav_actualtimea1 >= 1) {
                                Thread.sleep(delay);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        gtav_actualtimea1 = (1000 + gtav_newtime_a1 - new GregorianCalendar()
                                                .getTimeInMillis()) / 1000;
                                        buttona1.setText((getResources()
                                                .getString(R.string.game_gtav_a1))
                                                + ": "
                                                + String.valueOf(gtav_actualtimea1));
                                        gtav_timerHasStarted_a1 = true;

                                        if (gtav_actualtimea1 <= 0) {
                                            buttona1.setText((getResources()
                                                    .getString(R.string.game_gtav_a1))
                                                    + " (End)");
                                            gtav_timerHasStarted_a1 = false;
                                        }
                                    }
                                });
                            }
                        } catch (InterruptedException e) {
                        }
                    }
                };
                gtav_a1.start();
                break;
            case (2):
                final Button buttona2 = (Button) findViewById(R.id.buttona2);
                Thread gtav_a2 = new Thread() {

                    @Override
                    public void run() {
                        try {
                            while (gtav_actualtimea2 >= 1) {
                                Thread.sleep(delay);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        gtav_actualtimea2 = (1000 + gtav_newtime_a2 - new GregorianCalendar()
                                                .getTimeInMillis()) / 1000;
                                        buttona2.setText((getResources()
                                                .getString(R.string.game_gtav_a2))
                                                + ": "
                                                + String.valueOf(gtav_actualtimea2));
                                        gtav_timerHasStarted_a2 = true;

                                        if (gtav_actualtimea2 <= 0) {
                                            buttona2.setText((getResources()
                                                    .getString(R.string.game_gtav_a2))
                                                    + " (End)");
                                            gtav_timerHasStarted_a2 = false;
                                        }
                                    }
                                });
                            }
                        } catch (InterruptedException e) {
                        }
                    }
                };
                gtav_a2.start();
                break;
            case (3):
                final Button buttona3 = (Button) findViewById(R.id.buttona3);
                Thread gtav_a3 = new Thread() {

                    @Override
                    public void run() {
                        try {
                            while (gtav_actualtimea3 >= 1) {
                                Thread.sleep(delay);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        gtav_actualtimea3 = (1000 + gtav_newtime_a3 - new GregorianCalendar()
                                                .getTimeInMillis()) / 1000;
                                        buttona3.setText((getResources()
                                                .getString(R.string.game_gtav_a3))
                                                + ": "
                                                + String.valueOf(gtav_actualtimea3));
                                        gtav_timerHasStarted_a3 = true;

                                        if (gtav_actualtimea3 <= 0) {
                                            buttona3.setText((getResources()
                                                    .getString(R.string.game_gtav_a3))
                                                    + " (End)");
                                            gtav_timerHasStarted_a3 = false;
                                        }
                                    }
                                });
                            }
                        } catch (InterruptedException e) {
                        }
                    }
                };
                gtav_a3.start();
                break;
        }
    }

    public void refreshDota2() {
        final Button buttona1 = (Button) findViewById(R.id.buttona1);
        Thread dota2_a1 = new Thread() {

            @Override
            public void run() {
                try {
                    while (dota2_actualtimea1 >= 1) {
                        Thread.sleep(delay);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dota2_actualtimea1 = (1000 + dota2_newtime_a1 - new GregorianCalendar()
                                        .getTimeInMillis()) / 1000;
                                buttona1.setText((getResources()
                                        .getString(R.string.game_dota2_a1))
                                        + ": "
                                        + String.valueOf(dota2_actualtimea1));
                                dota2_timerHasStarted_a1 = true;

                                if (dota2_actualtimea1 <= 0) {
                                    buttona1.setText((getResources()
                                            .getString(R.string.game_dota2_a1))
                                            + " (End)");
                                    dota2_timerHasStarted_a1 = false;
                                }
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        dota2_a1.start();
    }

    public void refreshSC2(int SC2ID) {
        switch (SC2ID) {
            case (1):
                final Button buttona1 = (Button) findViewById(R.id.buttona1);
                Thread sc2_a1 = new Thread() {

                    @Override
                    public void run() {
                        try {
                            while (sc2_actualtimea1 >= 1) {
                                Thread.sleep(delay);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        sc2_actualtimea1 = (1000 + sc2_newtime_a1 - new GregorianCalendar()
                                                .getTimeInMillis()) / 1000;
                                        buttona1.setText((getResources()
                                                .getString(R.string.game_sc2_a1))
                                                + ": "
                                                + String.valueOf(sc2_actualtimea1));
                                        sc2_timerHasStarted_a1 = true;

                                        if (sc2_actualtimea1 <= 0) {
                                            buttona1.setText((getResources()
                                                    .getString(R.string.game_sc2_a1))
                                                    + " (End)");
                                            sc2_timerHasStarted_a1 = false;
                                        }
                                    }
                                });
                            }
                        } catch (InterruptedException e) {
                        }
                    }
                };
                sc2_a1.start();
                break;
            case (2):
                final Button buttona2 = (Button) findViewById(R.id.buttona2);
                Thread sc2_a2 = new Thread() {

                    @Override
                    public void run() {
                        try {
                            while (sc2_actualtimea2 >= 1) {
                                Thread.sleep(delay);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        sc2_actualtimea2 = (1000 + sc2_newtime_a2 - new GregorianCalendar()
                                                .getTimeInMillis()) / 1000;
                                        buttona2.setText((getResources()
                                                .getString(R.string.game_sc2_a2))
                                                + ": "
                                                + String.valueOf(sc2_actualtimea2));
                                        sc2_timerHasStarted_a2 = true;

                                        if (sc2_actualtimea2 <= 0) {
                                            buttona2.setText((getResources()
                                                    .getString(R.string.game_sc2_a2))
                                                    + " (End)");
                                            sc2_timerHasStarted_a2 = false;
                                        }
                                    }
                                });
                            }
                        } catch (InterruptedException e) {
                        }
                    }
                };
                sc2_a2.start();
                break;
            case (3):
                final Button buttona3 = (Button) findViewById(R.id.buttona3);
                Thread sc2_a3 = new Thread() {

                    @Override
                    public void run() {
                        try {
                            while (sc2_actualtimea3 >= 1) {
                                Thread.sleep(delay);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        sc2_actualtimea3 = (1000 + sc2_newtime_a3 - new GregorianCalendar()
                                                .getTimeInMillis()) / 1000;
                                        buttona3.setText((getResources()
                                                .getString(R.string.game_sc2_a3))
                                                + ": "
                                                + String.valueOf(sc2_actualtimea3));
                                        sc2_timerHasStarted_a3 = true;

                                        if (sc2_actualtimea3 <= 0) {
                                            buttona3.setText((getResources()
                                                    .getString(R.string.game_sc2_a3))
                                                    + " (End)");
                                            sc2_timerHasStarted_a3 = false;
                                        }
                                    }
                                });
                            }
                        } catch (InterruptedException e) {
                        }
                    }
                };
                sc2_a3.start();
                break;
            case (4):
                final Button buttona4 = (Button) findViewById(R.id.buttona4);
                Thread sc2_a4 = new Thread() {

                    @Override
                    public void run() {
                        try {
                            while (sc2_actualtimea4 >= 1) {
                                Thread.sleep(delay);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        sc2_actualtimea4 = (1000 + sc2_newtime_a4 - new GregorianCalendar()
                                                .getTimeInMillis()) / 1000;
                                        buttona4.setText((getResources()
                                                .getString(R.string.game_sc2_a4))
                                                + ": "
                                                + String.valueOf(sc2_actualtimea4));
                                        sc2_timerHasStarted_a4 = true;

                                        if (sc2_actualtimea4 <= 0) {
                                            buttona4.setText((getResources()
                                                    .getString(R.string.game_sc2_a4))
                                                    + " (End)");
                                            sc2_timerHasStarted_a4 = false;
                                        }
                                    }
                                });
                            }
                        } catch (InterruptedException e) {
                        }
                    }
                };
                sc2_a4.start();
                break;
        }
    }

    public void refreshCUBE(int CUBEID) {
        switch (CUBEID) {
            case (1):
                final Button buttona1 = (Button) findViewById(R.id.buttona1);
                Thread cube_a1 = new Thread() {

                    @Override
                    public void run() {
                        try {
                            while (cube_actualtimea1 >= 1) {
                                Thread.sleep(delay);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        cube_actualtimea1 = (1000 + cube_newtime_a1 - new GregorianCalendar()
                                                .getTimeInMillis()) / 1000;
                                        buttona1.setText((getResources()
                                                .getString(R.string.game_cube_a1))
                                                + ": "
                                                + String.valueOf(cube_actualtimea1));
                                        cube_timerHasStarted_a1 = true;

                                        if (cube_actualtimea1 <= 0) {
                                            buttona1.setText((getResources()
                                                    .getString(R.string.game_cube_a1))
                                                    + " (End)");
                                            cube_timerHasStarted_a1 = false;
                                        }
                                    }
                                });
                            }
                        } catch (InterruptedException e) {
                        }
                    }
                };
                cube_a1.start();
                break;
            case (2):
                final Button buttona2 = (Button) findViewById(R.id.buttona2);
                Thread cube_a2 = new Thread() {

                    @Override
                    public void run() {
                        try {
                            while (cube_actualtimea2 >= 1) {
                                Thread.sleep(delay);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        cube_actualtimea2 = (1000 + cube_newtime_a2 - new GregorianCalendar()
                                                .getTimeInMillis()) / 1000;
                                        buttona2.setText((getResources()
                                                .getString(R.string.game_cube_a2))
                                                + ": "
                                                + String.valueOf(cube_actualtimea2));
                                        cube_timerHasStarted_a2 = true;

                                        if (cube_actualtimea2 <= 0) {
                                            buttona2.setText((getResources()
                                                    .getString(R.string.game_cube_a2))
                                                    + " (End)");
                                            cube_timerHasStarted_a2 = false;
                                        }
                                    }
                                });
                            }
                        } catch (InterruptedException e) {
                        }
                    }
                };
                cube_a2.start();
                break;
            case (3):
                final Button buttona3 = (Button) findViewById(R.id.buttona3);
                Thread cube_a3 = new Thread() {

                    @Override
                    public void run() {
                        try {
                            while (cube_actualtimea3 >= 1) {
                                Thread.sleep(delay);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        cube_actualtimea3 = (1000 + cube_newtime_a3 - new GregorianCalendar()
                                                .getTimeInMillis()) / 1000;
                                        buttona3.setText((getResources()
                                                .getString(R.string.game_cube_a3))
                                                + ": "
                                                + String.valueOf(cube_actualtimea3));
                                        cube_timerHasStarted_a3 = true;

                                        if (cube_actualtimea3 <= 0) {
                                            buttona3.setText((getResources()
                                                    .getString(R.string.game_cube_a3))
                                                    + " (End)");
                                            cube_timerHasStarted_a3 = false;
                                        }
                                    }
                                });
                            }
                        } catch (InterruptedException e) {
                        }
                    }
                };
                cube_a3.start();
                break;
            case (4):
                final Button buttonb1 = (Button) findViewById(R.id.buttonb1);
                Thread cube_b1 = new Thread() {

                    @Override
                    public void run() {
                        try {
                            while (cube_actualtimeb1 >= 1) {
                                Thread.sleep(delay);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        cube_actualtimeb1 = (1000 + cube_newtime_b1 - new GregorianCalendar()
                                                .getTimeInMillis()) / 1000;
                                        buttonb1.setText((getResources()
                                                .getString(R.string.game_cube_b1))
                                                + ": "
                                                + String.valueOf(cube_actualtimeb1));
                                        cube_timerHasStarted_b1 = true;

                                        if (cube_actualtimeb1 <= 0) {
                                            buttonb1.setText((getResources()
                                                    .getString(R.string.game_cube_b1))
                                                    + " (End)");
                                            cube_timerHasStarted_b1 = false;
                                        }
                                    }
                                });
                            }
                        } catch (InterruptedException e) {
                        }
                    }
                };
                cube_b1.start();
                break;
            case (5):
                final Button buttonb2 = (Button) findViewById(R.id.buttonb2);
                Thread cube_b2 = new Thread() {

                    @Override
                    public void run() {
                        try {
                            while (cube_actualtimeb2 >= 1) {
                                Thread.sleep(delay);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        cube_actualtimeb2 = (1000 + cube_newtime_b2 - new GregorianCalendar()
                                                .getTimeInMillis()) / 1000;
                                        buttonb2.setText((getResources()
                                                .getString(R.string.game_cube_b2))
                                                + ": "
                                                + String.valueOf(cube_actualtimeb2));
                                        cube_timerHasStarted_b2 = true;

                                        if (cube_actualtimeb2 <= 0) {
                                            buttonb2.setText((getResources()
                                                    .getString(R.string.game_cube_b2))
                                                    + " (End)");
                                            cube_timerHasStarted_b2 = false;
                                        }
                                    }
                                });
                            }
                        } catch (InterruptedException e) {
                        }
                    }
                };
                cube_b2.start();
                break;
            case (6):
                final Button buttonb3 = (Button) findViewById(R.id.buttonb3);
                Thread cube_b3 = new Thread() {

                    @Override
                    public void run() {
                        try {
                            while (cube_actualtimeb3 >= 1) {
                                Thread.sleep(delay);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        cube_actualtimeb3 = (1000 + cube_newtime_b3 - new GregorianCalendar()
                                                .getTimeInMillis()) / 1000;
                                        buttonb3.setText((getResources()
                                                .getString(R.string.game_cube_b3))
                                                + ": "
                                                + String.valueOf(cube_actualtimeb3));
                                        cube_timerHasStarted_b3 = true;

                                        if (cube_actualtimeb3 <= 0) {
                                            buttonb3.setText((getResources()
                                                    .getString(R.string.game_cube_b3))
                                                    + " (End)");
                                            cube_timerHasStarted_b3 = false;
                                        }
                                    }
                                });
                            }
                        } catch (InterruptedException e) {
                        }
                    }
                };
                cube_b3.start();
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && mainisopen == false) {
            mainisopen = true;
            setContentView(R.layout.activity_main_game);
            setTitle("Gameplay Timer");
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
